# Copyright 1999-2010 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2

DESCRIPTION="Adobes free ICC profiles, CMYK and RGB"
HOMEPAGE="http://www.adobe.com/"
SRC_URI="ftp://ftp.adobe.com/pub/adobe/iccprofiles/win/AdobeICCProfilesCS4Win_end-user.zip"
LICENSE="Adobe"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE=""
DEPEND=""
S=${WORKDIR}/Adobe\ ICC\ Profiles

ICCDIR="/usr/share/color/icc/adobe/"

src_compile() {
	ewarn "Installing profiles in ${ICCDIR}"
}

src_install() {
	dodir ${ICCDIR}/RGB/
	insinto ${ICCDIR}/RGB/
	doins Adobe\ ICC\ Profiles\ \(end-user\)/RGB/*

	dodir ${ICCDIR}/CMYK/
	insinto ${ICCDIR}/CMYK/
	doins Adobe\ ICC\ Profiles\ \(end-user\)/CMYK/*

	dodoc Adobe\ ICC\ Profiles\ \(end-user\)/*.pdf
}

