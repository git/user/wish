# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit eutils
EAPI=2

DESCRIPTION="Apple's Time Machine alternative for Linux"
HOMEPAGE="http://code.google.com/p/flyback/"
SRC_URI="http://flyback.googlecode.com/files/flyback-lucid_0.6.5-1_all.deb"

IUSE=""
SLOT="0"
LICENSE="UNKNOWN"
KEYWORDS="~amd64 ~x86"
RESTRICT="strip mirror"

DEPEND=">=dev-lang/python-3.1.2
	>=dev-util/glade-3.6.7[gnome,python]
	>=gnome-base/libglade-2.6.4
	>=dev-vcs/git-1.7.2.2"
RDEPEND="${DEPEND}"

src_unpack() {
    unpack ${A}
    unpack ./data.tar.gz
    epatch "${FILESDIR}/${P}-location.patch"
}

src_install() {
    BASEDIR=usr/share
    INSTDIR=/opt

    // Python files
    insinto "${INSTDIR}/flyback"
    for file in "${BASEDIR}"/flyback/*;
    do
       doins "${file}"
    done

    // Glade files
    insinto "${INSTDIR}/flyback/glade"
    for file in "${BASEDIR}"/flyback/glade/*;
    do
       doins "${file}"
    done

    // Executable file
    exeinto "${INSTDIR}/flyback"
    doexe "${BASEDIR}/flyback/flyback.py"

    // Changelog
    dodoc "${BASEDIR}/doc/flyback/changelog.Debian.gz"

    // Icon
    exeinto "/usr/share/applications"
    doexe "${BASEDIR}/applications/flyback.desktop"
}
