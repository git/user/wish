# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

DESCRIPTION="Various Bulgarian <=> English dictionary files, needed for gbgoffice and kbgoffice"
HOMEPAGE="http://gbgoffice.info"
SRC_URI="mirror://sourceforge/bgoffice/Full%20Pack%20of%20Dictionaries/1.0/full-pack.tar.bz2"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"

# TODO: depend on kbgoffice or gbgoffice
DEPEND=""
      
src_install() {
   einfo "Installing full dictionary pack..."
   insinto "/usr/share/bgoffice"
   for file in ${WORKDIR}/full-pack/data/*;
   do
      doins ${file}
   done
}
