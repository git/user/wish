# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

inherit qt4-r2

DESCRIPTION="A QT-based Bulgarian <=> English dictionary"
HOMEPAGE="http://bgoffice.sourceforge.net/"
SRC_URI="mirror://sourceforge/bgoffice/BG%20Office%20Assistant/2.1/${P}.tar.gz"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=x11-libs/qt-core-4.6.3
	>=x11-libs/qt-gui-4.6.3
	>=app-dicts/bgoffice-dictfiles-1.0"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${P}/src"

src_prepare() {
	cd "${S}"

        qt4-r2_src_prepare
	epatch "${FILESDIR}/${P}-fix-paths.patch"
}

src_configure() {
	eqmake4 -config release || die 'eqmake -config failed'
}

src_install() {
	qt4-r2_src_install
}




