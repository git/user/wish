# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit eutils gnome2

DESCRIPTION="A very nice GTK based Bulgarian <=> English dictionary"
HOMEPAGE="http://gbgoffice.info"
SRC_URI="http://openfmi.net/frs/download.php/341/${P}.tar.gz"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"
IUSE="light"

DEPEND=">=dev-libs/libsigc++-2.2.3
      >=dev-cpp/gtkmm-2.16.0
      >=dev-cpp/glibmm-2.20.1
      >=app-dicts/bgoffice-dictfiles-1.0"
      
src_unpack() {
   einfo "Source unpacking..."
   if [ "${A}" != "" ]; then
        unpack ${A} || die "Failed to unpack needed sources!"
   fi
   cd ${S}

   # The app is unsupported, so lot's of patches should be applied
   epatch "${FILESDIR}"/${PV}-01_re-autogen.patch
   epatch "${FILESDIR}"/${PV}-02_gcc-4.3.patch
   epatch "${FILESDIR}"/${PV}-03_gcc-4.3-const-chars.patch
   epatch "${FILESDIR}"/${PV}-04_sigc-deprecated-fix.patch
   epatch "${FILESDIR}"/${PV}-05_convert-const.patch
   epatch "${FILESDIR}"/${PV}-06_fsf-address-fix.patch
   epatch "${FILESDIR}"/${PV}-07_gtk_buttonbox-fix.patch
}

src_compile() {
   local myconf
   if use light; then
      myconf="${myconf} --enable-light-version"
   fi
   einfo "Configuring..."
    if [ -x ./configure ]; then
      econf ${myconf} || die "Configuring failed!"
   fi
   einfo "Building..."
   if [ -f Makefile ] || [ -f GNUmakefile ] || [ -f makefile ]; then
      emake || die "Building failed!"
   fi
}

src_install() {
   # Installing dict
   einfo "Installing gbgoffice..."
   emake DESTDIR="${D}" install || die "Install failed!"
   
   # Install GNOME icon
   insinto "/usr/share/applications"
   doins "${FILESDIR}"/gbgoffice.desktop
}
