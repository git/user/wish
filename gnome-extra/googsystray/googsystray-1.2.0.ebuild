# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"
PYTHON_DEPEND="2:2.6"

inherit distutils

DESCRIPTION="GNOME Google/Gmail System tray application."
HOMEPAGE="http://googsystray.sourceforge.net/"
SRC_URI="mirror://sourceforge/googsystray/${P}/${P}.tar.gz"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64 ~ppc"
IUSE=""

DEPEND="=dev-lang/python-2.6*
	>=dev-python/pygtk-2.14.1-r1
	>=dev-python/notify-python-0.1.1-r1"
      
src_install() {
	distutils_src_install
}
