# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit base flag-o-matic autotools eutils

EAPI="3"

DESCRIPTION="Tools for certain user account management tasks"
HOMEPAGE="https://fedorahosted.org/usermode/"
SRC_URI="https://fedorahosted.org/releases/u/s/${PN}/${P}.tar.xz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="debug selinux"
RDEPEND="=dev-libs/glib-2*
	=x11-libs/gtk+-2*
	=gnome-base/libglade-2*
	sys-apps/attr
	x11-libs/libSM
	dev-util/desktop-file-utils
	sys-libs/system-config-base
	>=sys-libs/pam-0.75
	sys-apps/shadow
	dev-perl/XML-Parser
	sys-libs/libuser"
DEPEND="${RDEPEND}
	sys-devel/gettext"

pkg_setup() {
	if ! built_with_use sys-apps/shadow pam; then
		eerror "${CATEGORY}/${PN} depends on the chfn and passwd PAM service"
		eerror "configuration files installed by sys-apps/shadow with PAM"
		eerror "enabled."
		eerror "Please re-install sys-apps/shadow with the pam USE flag"
		eerror "enabled."
		die "sys-apps/shadow was built without PAM support."
	fi
}

src_prepare() {
	cd "${S}"

	# Change vendor prefix of desktop file from redhat to gentoo
	sed -i -e "s:^\(VENDOR=\).*:\1gentoo:g" Makefile.am

	# Invalid categories in desktop-file-utils-0.11 (#153395)
	sed -i \
		-e "/AdvancedSettings/d" \
		-e "/Application/d" \
		-e "/X-Red-Hat-Base/d" \
		Makefile.am

	# Invalid naming of icons
	sed -i \
		-e "s:\.png::g" \
		*.desktop.in

	eautoreconf
}

src_configure() {
	append-ldflags -Wl,-z,now

	econf \
		$(use_with selinux) \
		$(use_enable debug) \
		|| die "econf failed"
}

src_compile() {
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	# This needs to be suid, it's the main interface with suid-requiring stuff
	fperms 4711 /usr/sbin/userhelper

	# Don't install a shutdown executable, it will be preferred over /sbin/
	rm "${D}"/usr/bin/shutdown
}

